import { mock, anything, when } from 'ts-mockito';
import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, HttpService, HttpStatus } from '@nestjs/common';
import { MicroserviceModule } from '../../src/microservice.module';
import { of } from 'rxjs';

describe('Camera Iot microservice', () => {
  let mockedHttpService: HttpService;
  let app: INestApplication;

  beforeEach(async () => {
    const httpMock = { post: () => of(null) };

    mockedHttpService = mock(HttpService);

    when(mockedHttpService.post(anything(), anything())).thenReturn(of(null));

    const moduleFixture = await Test.createTestingModule({
      imports: [MicroserviceModule],
    })
      .overrideProvider(HttpService)
      .useValue(httpMock)
      .compile();

    app = moduleFixture.createNestApplication();

    await app.init();

    await app.listenAsync(4000);
  });

  afterEach(async () => {
    await app.close();
  });

  describe('Enable user stream', () => {
    it('should call streaming microservice disable endpoint', async () => {
      await request(app.getHttpServer())
        .post('/camera-iot/enable')
        .set('Accept', 'application/json')
        .send({
          userId: 'userTest',
          url: 'url',
        })
        .expect(HttpStatus.CREATED);
    });
  });

  describe('disable user stream', () => {
    it('should call streaming microservice disable endpoint', async () => {
      return request(app.getHttpServer())
        .post('/camera-iot/disable')
        .set('Accept', 'application/json')
        .send({
          userId: 'userTest',
          url: 'url',
        })
        .expect(HttpStatus.CREATED);
    });
  });
});
