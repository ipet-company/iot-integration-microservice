import { Module, HttpModule } from '@nestjs/common';
import { CameraIotModule } from './camera-iot/camera-iot.module';

@Module({
  imports: [CameraIotModule],
  controllers: [],
  providers: [],
})
export class MicroserviceModule {}
