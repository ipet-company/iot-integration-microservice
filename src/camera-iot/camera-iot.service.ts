import { Injectable, HttpService } from '@nestjs/common';
import { Stream } from '../interfaces/stream.interface';

@Injectable()
export class CameraIotService {
  private readonly baseUrl: string;

  constructor(private readonly httpService: HttpService) {
    this.baseUrl =
      process.env.NODE_ENV === 'production'
        ? 'https://ipet-streaming-microservice.herokuapp.com'
        : 'http://localhost:8820';
  }

  async enableStream(stream: Stream): Promise<void> {
    console.log(this.baseUrl);
    await this.httpService
      .post(`${this.baseUrl}/streaming/enable`, stream)
      .toPromise();
  }

  async disableStream(stream: Stream): Promise<void> {
    await this.httpService
      .post(`${this.baseUrl}/streaming/disable`, stream)
      .toPromise();
  }
}
