import { mock, instance, verify, anything, when, resetCalls } from 'ts-mockito';
import { CameraIotService } from './camera-iot.service';
import { HttpService } from '@nestjs/common';
import { Stream } from '../interfaces/stream.interface';
import { of } from 'rxjs';

describe('cameraIotService.unit', () => {
  let mockedHttpService: HttpService;
  let cameraIotService: CameraIotService;

  const streamTest: Stream = {
    userId: 'userTest',
    url: 'url',
  };

  beforeAll(() => {
    mockedHttpService = mock(HttpService);
    cameraIotService = new CameraIotService(instance(mockedHttpService));

    when(mockedHttpService.post(anything(), anything())).thenReturn(of(null));
  });

  describe('enableStream()', () => {
    it('should call enable streaming microservice endpoint with stream info', async () => {
      resetCalls(mockedHttpService);

      await cameraIotService.enableStream(streamTest);

      verify(
        mockedHttpService.post(
          'http://localhost:8820/streaming/enable',
          anything(),
        ),
      ).once();
    });
  });

  describe('disableStream()', () => {
    it('should call disable streaming microservice endpoint with stream info', async () => {
      resetCalls(mockedHttpService);

      await cameraIotService.disableStream(streamTest);

      verify(
        mockedHttpService.post(
          'http://localhost:8820/streaming/disable',
          anything(),
        ),
      ).once();
    });
  });
});
