import { Module, HttpModule } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { CameraIotService } from './camera-iot.service';
import { CameraIotController } from './camera-iot.controller';
import { Logger } from '../logger/logger';

@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
  ],
  controllers: [CameraIotController],
  providers: [CameraIotService, Logger, String],
})
export class CameraIotModule {}
