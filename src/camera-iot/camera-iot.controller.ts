import {
  Controller,
  Post,
  Body,
  InternalServerErrorException,
} from '@nestjs/common';
import { CameraIotService } from './camera-iot.service';
import { Stream } from './../interfaces/stream.interface';
import { Logger } from '../logger/logger';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('camera-iot')
@Controller('camera-iot')
export class CameraIotController {
  constructor(
    private readonly cameraIotService: CameraIotService,
    private readonly logger: Logger,
  ) {}

  @Post('enable')
  async enableUserStream(@Body() stream: Stream): Promise<void> {
    this.logger.info(
      'Stream enable requested',
      `${CameraIotController.name}.${this.enableUserStream.name}`,
    );
    try {
      await this.cameraIotService.enableStream(stream);
    } catch (error) {
      this.logger.error(
        'Request to streaming microservice endpoint enable failed',
        null,
        null,
        error,
        { stream },
      );

      throw new InternalServerErrorException('Request failed. Try again later');
    }
  }

  @Post('disable')
  async disableUserStream(@Body() stream: Stream): Promise<void> {
    this.logger.info(
      'Stream disable requested',
      `${CameraIotController.name}.${this.enableUserStream.name}`,
    );
    try {
      await this.cameraIotService.disableStream(stream);
    } catch (error) {
      this.logger.error(
        'Request to streaming microservice endpoint disable failed',
        null,
        null,
        error,
        { stream },
      );

      throw new InternalServerErrorException('Request failed. Try again later');
    }
  }
}
