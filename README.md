# Ipet - Streaming Microservice

## Description

This microservice has the responsibility to manage all  live streams that take place within the social network or in a user's private environment. [Roadmap]

Its functions are:

- Connect a single stream using IOT Integration Microservice and manage the stream life in the application

## Methods

bla bla

## Installation

```bash
npm install
```

## Running the microservice

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## License

  Streaming Microservice is [MIT licensed](LICENSE).
